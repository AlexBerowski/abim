server {
	listen 80;
	listen [::]:80;
	client_max_body_size 64M; # to upload large books
	server_name lbry.berowski.com;
	location / {
		proxy_pass http://127.0.0.1:8080;
	}
}
