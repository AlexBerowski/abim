server {
	listen 80 ;
	listen [::]:80 ;
	server_name mail.berowski.com ;
	root /srv/mail ;
	index index.html index.htm index.nginx-debian.html ;
	location / {
		try_files $uri $uri/ =404 ;
	}
}
