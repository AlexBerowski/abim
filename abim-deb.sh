#!/bin/bash
#A script to install ABIM from scratch.
#scp into server; start from /root directoary.

##Package Installation
apt update -y && apt upgrade -y
apt install nginx python3-certbot-nginx rsync apache2-utils git fcgiwrap cgit calibre figlet -y
apt autoremove -y

#Gather User Variables
echo 'Enter Source Server for Site files (user@host):'
read SOURCE
echo 'Enter Location of Main Site files on Source Server (use a trailing slash, hidden files will beignored):'
read MAIN
echo 'Enter Location of Database files on Source Server (use a trailing slash, hidden files will be ignored):'
read DATA
echo 'Enter Location of Library files on Source Server (use a trailing slash, hidden files will be ignored):'
read BOOKS


Bash configuration
sed -in 's/# export/export/' /root/.bashrc
sed -in 's/# eval/eval/' /root/.bashrc
sed -in 's/# alias/alias/' /root/.bashrc
sed -in '/vultr/d' /root/.bashrc
echo 'set -o vi' >> /root/.bashrc
echo "alias s='systemctl'" >> /root/.bashrc
echo "alias j='journalctl -xe'" >> /root/.bashrc
echo "alias updatemain='rsync -rtvP --exclude=".*" $SOURCE:$MAIN /srv/www'" >> /root/.bashrc
echo "alias updatedb='rsync -rtvP --exclude=".*" $SOURCE:$DATA /srv/db'" >> /root/.bashrc
echo "alias updatelbry='rsync -rtvP --exclude=".*" $SOURCE:$BOOKS /srv/lbry'" >> /root/.bashrc
source root/.bashrc

#Nginx Configuration
mksite(){
	local SITE=$1
	mv ./$SITE /etc/nginx/sites-available/
	ln -s /etc/nginx/sites-available/$SITE /etc/nginx/sites-enabled/
}

mksite berowski.com
mkdir /srv/www
mksite db.berowski.com
mkdir /srv/db
mksite git.berowski.com
mkdir /srv/git
mksite mail.berowski.com
mkdir /srv/mail
mksite lbry.berowski.com
mkdir /srv/lbry
mksite matrix.berowski.com

systemctl restart nginx

sed -in '/server_tokens/ {s/# //}' /etc/nginx/nginx.conf

#Firewall Configuration
ufw allow 80
ufw allow 443

#crontab -e
#crontab -l > cron.tmp
echo '0 0 1 * * certbot --nginx renew' >> cron.tmp
echo '0 0 * * * updatemain' >> cron.tmp
echo '0 0 * * * updatedb' >> cron.tmp
echo '0 0 * * * updatelbry' >> cron.tmp
crontab cron.tmp
rm -f cron.tmp

#SSH with Source Server
ssh-keygen -f ~/.ssh/id_rsa -P ""
ssh-copy-id $SOURCE

#Main Site Sync
updatemain

#Database Configuration
echo 'Making "alex" user for Accounts Database.'
htpasswd -c /etc/nginx/myusers alex #user authentication
updatedb

#Git Configuration
echo 'Making "git" user for system.'
useradd -d /srv/git git
chown git:git -R /srv/git
su -l --command='sh git-config.sh'
cd

#Cgit Configuration
sed -in '/root/ {s/\/srv\/git/\/usr\/share\/cgit/}' /etc/nginx/sites-available/git.berowski.com
sed -in '/index/d' /etc/nginx/sites-available/git.berowski.com
sed -in '/root/a \\ttry_files $uri @cgit ;' /etc/nginx/sites-available/git.berowski.com
sed -in '/location/{n;d}' /etc/nginx/sites-available/git.berowski.com
sed -in '/location/{s/\//@cgit/}' /etc/nginx/sites-available/git.berowski.com
sed -in '/location/a \\t\tfastcgi_pass unix:\/run\/fcgiwrap.socket;' /etc/nginx/sites-available/git.berowski.com
sed -in '/location/a \\t\tfastcgi_param QUERY_STRING $query_string;' /etc/nginx/sites-available/git.berowski.com
sed -in '/location/a \\t\tfastcgi_param PATH_INFO $request_uri;' /etc/nginx/sites-available/git.berowski.com
sed -in '/location/a \\t\tfastcgi_param SCRIPT_FILENAME \/usr\/lib\/cgit\/cgit.cgi;' /etc/nginx/sites-available/git.berowski.com
sed -in '/location/a \\t\tinclude fastcgi_params;' /etc/nginx/sites-available/git.berowski.com

systemctl restart nginx

mv -f ~/ABIM/cgitrc /etc
mv -f ~/ABIM/cgit-dark.css  /usr/share/cgit/

usermod -c "Alex Berowski" git

#cd /srv/git
#mkdir hooks
#cp ~/ABIM/post-receive hooks/
#chmod +x hooks/post-receive

#Calibre Configuration
updatelbry
calibredb --with-library /srv/lbry add /srv/lbry/*
echo 'Making "alex" user for Calibre.'
calibre-server --manage-users
mv calibre-server.service /etc/systemd/system/
systemctl enable --now calibre-server

#HTTPS Configuration
certbot --nginx --register-unsafely-without-email

#Email Server Configuration
#All credit for this part goes to Luke Smith. He's a life saver.
ufw allow 25
ufw allow 587
ufw allow 993
curl -LO lukesmith.xyz/emailwiz.sh
sh emailwiz.sh
useradd -G mail -m alex
echo 'Making "alex" user for system and mail. Enter password:'
passwd alex
less emailGuide

#Finish Message
figlet -c -k Done!
