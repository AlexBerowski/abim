server {
	listen 80 ;
	listen [::]:80 ;
	server_name db.berowski.com ;
	root /srv/db ;
	index index.html index.htm index.nginx-debian.html ;
	auth_basic "What's the Password" ;
	auth_basic_user_file /etc/nginx/myusers ;
	location / {
		try_files $uri $uri/ =404 ;
	}
}
