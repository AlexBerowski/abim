server {
	listen 80 ;
	listen [::]:80 ;
	server_name berowski.com ;
	root /srv/www ;
	index index.html index.htm index.nginx-debian.html ;
#	kill cache > lower performance, rapid updates, ideal for small low-traffic site.
	expires -1;
	location / {
		try_files $uri $uri/ =404 ;
	}
}
